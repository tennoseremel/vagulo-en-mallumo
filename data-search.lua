obj {
	nam = "change";
	disp = "ŝanĝi";
	dsc = [[Ŝajnas, ke mi sukcese iras, sed nenio {change|ŝanĝi}ĝas.
			Eble mi devas iri malantaŭen, se mi povos trovi la lokon. Aŭ eble ne…]];
	act = [[Nenio okazis.]];
};

room {
	pic = "gfx/mallumo.png";
	nam = "search";
	disp = "Ie";
	dsc = [[Mallume. Mi ne povas trovi ion ajn, malgraŭ daŭra irado.]];
	decor = function(s)
		if _("tiktako").tik_tak_count > 0 then
			p [[{tiktako|Tik-tak}…]];
		end;
	end;
	obj = {"change", "tiktako"};
	tiktako_increase = true;
	way = {path {"Loko de vekiĝo", "main"}};
};
