obj {
	nam = "room_of_fear_armchair";
	disp = "brakseĝo";
	dsc = [[Mola {room_of_fear_armchair|brakseĝo} staras en la centro de la ĉambro.]];
	is_used = false;
	act = function (s)
		if not s.is_used then
			p [[Mi sidiĝas sur la brakseĝon, kiu estas tiom komforta, ke mi ne volas iri ien ajn.
				Mi ankaŭ ne povas vidi ion ajn tra la fenestro, do mi fermas la okulojn kaj iomete dormetas.
				^^Mi ne certas, kiom da tempo pasis, kiam mi startas pensi.
				^^Kiu mi estas?]];
		end;

		local messages = {
			[1] = "Kio mi estas?",
			[2] = "Kie mi estas?",
			[3] = "Kion mi deziras?",
			[4] = "Ĉu mi neniam eliĝos?",
			[5] = "Kion mi faras?",
			[6] = "Tio ne sukcesos.",
			[7] = "Prenu tion kiel signo.",
			[8] = "Mi ĉiam atendas.",
			[9] = "Bruligu!",
			[10] = "Vi ne forkuros!",
			[11] = "Ĉu homa vivo estas doloro?",
			[12] = "Kiam mi ekzistas?",
			[13] = "Kial mi vivas?",
			[14] = "Kio estas feliĉo?",
			[15] = "Kiun vi povis fidi?",
			[16] = "Ĉu vi vere ekzistas?",
			[17] = "Ĉu iu ajn bezonas min?",
			[18] = "Vi jam forĵetis preskaŭ ĉion.",
			[19] = "Mi ne forgesos.",
			[20] = "Ĉu vi promesas?"
		};
		for i = 1, 5 do
			p('^^', table.remove(messages, rnd(#messages)));
		end;

		if not s.is_used then
			s.is_used = true;
			p [[^^Aŭ eble tio ne estas miaj pensoj, eble mi nur aŭdas senkorpajn voĉojn…
				Mi volas foriri de ĉi tiu ĉambro. Mi ne ŝatas ĝin.]];
		end;
	end;
};

obj {
	nam = "room_of_fear_window";
	disp = "fenestro";
	dsc = [[Post la brakseĝo estas larĝa {room_of_fear_window|fenestro}.]];
	act = [[Mi ne povas vidi ion ajn tra polva ŝtormo.]];
};

room {
	pic = "gfx/cxambro-de-timo.png";
	nam = "room_of_fear";
	disp = function(s)
		if s:visited() then
			p [[Ĉambro de timo]];
		else
			p [[La unua pordo]];
		end;
	end;
	title = "Ĉambro de timo";
	dsc = [[Ĉi tiu ĉambro estas stranga.
			Ĝi dronas en mallumo, kaj mi eĉ ne certas, ke muroj ekzistas ĉi tie, tamen kelkajn aĵojn mi ial povas vidi.
			^^Mi volas forkuri de ĉi tiu ĉambro. Ne pro mallumo, sed io alia.]];
	decor = [[{tiktako|Tik-tak}…]];
	obj = {"tiktako", "room_of_fear_armchair", "room_of_fear_window"};
	tiktako_increase = true;
	way = {path {"Malantaŭen", "stairs"}};
};
