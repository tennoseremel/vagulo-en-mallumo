obj {
	nam = "mallumo";
	disp = "mallumo";
	known = false;
	transformed = false;
	tak = [[Mi eletendas la manon kaj facile prenas iom da mallumo.]];
	inv = function(s)
		if not s.known and not s.transformed then
			s.known = true;
		end
		return "Ĝi ŝajnas mola. Amuze";
	end;
	use = function(s, other)
		if other.nam == "change" then
			purge(other);
			s.transformed = true;
			s.disp = "lumo";
			p [[Malrapide aperis lumo, kaj mi nun povas vidi.]];
			if s.known then
				INFO_central_room = "library";
			else
				INFO_central_room = "whiteroom";
			end;
			walk(INFO_central_room);
		elseif other.nam == "water" and not other.hot then
			other.hot = true;
			p [[Io okazis.]];
		else
			p [[Nenio okazis.]];
		end;
	end;
};

obj {
	nam = "tiktako";
	disp = "tik-tak";
	tik_tak_count = 0;
	act = function(s)
		local messages = {
			[1] = "Tik-tak…",
			[2] = "Hm…",
			[3] = "Strange…",
			[4] = "Kio ĝuste tiktakas?",
		};
		local current_room = here();
		if current_room.tiktako_increase then
			s.tik_tak_count = s.tik_tak_count + 1;
			current_room.tiktako_increase = nil;
		end;
		return messages[rnd(4)];
	end;
};

room {
	pic = "gfx/mallumo.png";
	nam = "main";
	disp = function(s)
		if _("mallumo").known then
			return "Konata mallumo";
		else
			return "Nekonata mallumo";
		end;
	end;
	dsc = [[Mi ne scias, kie mi troviĝas. Eble nenie, ĉar mi sentas nenion. Nu, preskaŭ nenion.]];
	decor = function(s)
		p [[Mi aŭdas unu sonon: {tiktako|tik-tak}, tik-tak…
			Nenia alia sono ekzistas nun, do mi preskaŭ ne rimarkas ĝin.
			Eble aliaj sonoj neniam ekzistis. Eble la tiktako ankaŭ ne ekzistas.]];
		if have "mallumo" then
			local mallumo = _("mallumo");
			if mallumo.transformed then
				p [[^^Malgraŭ ĉio mallumo estas ĉie.]];
			else
				p [[^^Mallumo estas ĉie.]];
			end;
			if mallumo.known then
				p [[Iamaniere ĝi ŝajnas mola.]];
			else
				p [[Ĝi ne provas timigi min.]];
			end;
		else
			p [[^^{mallumo|Mallumo} estas ĉie.]];
		end;
		p [[^^Eble mi devas iri… Kien?]];
		local central_room = get_central_nam();
		if central_room == "whiteroom" then
			p [[En la blankan ĉambron.]];
		elseif central_room == "library" then
			p [[En la librejon.]];
		else
			p [[Ien.]];
		end;
	end;
	obj = {"tiktako", "mallumo"};
	tiktako_increase = true;
	way = {path {nam = "#tocentral", get_central_disp, get_central_nam}};
	exit = [[Mi provas paŝi.]];
	enter = function(s)
		if have "clothes" then
			remove("clothes"); -- remove from inventory
			p "Mi ie perdis la veston…";
		end;
	end;
};
