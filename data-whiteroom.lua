obj {
	nam = "armchair";
	disp = "brakseĝo";
	dsc = [[Aspekte komforta {armchair|brakseĝo} staras dekstre de mi.]];
	act = [[Eble poste.]];
};

obj {
	nam = "table_whiteroom";
	disp = "tablo";
	dsc = [[Ligna {table_whiteroom|tablo} staras apud ĝi.]];
	act = [[Ĝi aspektas tre peza. Mi ŝatas ĝin.]];
	obj = {"notebook"};
};

obj {
	nam = "notebook";
	disp = "notlibro";
	dsc = [[Sur la tablo kuŝas {notebook|notlibro}.]];
	inv = [[«Ĝis la estonta revido», ĝi diras.]];
	tak = [[Mi prenas la notlibron.]];
	use = function (s, other)
		if other.nam == "table_whiteroom" then
			p [[Mi metis ĝin sur la tablon.]];
			place(s, "table_whiteroom");
		elseif other.nam == "mirror" then
			if other.notebook_activated then
				p [[Nenio okazis.]]
			else
				other.notebook_activated = true;
				p [[Neatendite la juna spegulaĵo elŝovas la manon el spegulo kaj kaptas la notlibron.
					Mi tiras la notlibron, kaj la mano revenas al la transspegulo.
					La spegulaĵo elŝovas la langon.]];
				s.inv = [[«Ĉu vi ŝatis ĉi tiun vivon, kara fratinjo?», ĝi diras.]];
			end;
		else
			p [[Ŝajne, ne.]]
		end
	end;
};

obj {
	nam = "carpet";
	disp = "tapiŝo";
	dsc = function(s)
		p [[Nigra {carpet|tapiŝo} kovras la tutan plankon.]];
		if not INFO_clothes_on then
			p [[Mi sentas ĝin per miaj piedoj.]];
		end;
	end;
	act = function(s)
		if INFO_clothes_on then
			p [[Ĝi ial ne havas ornamojn.]]
		else
			p [[Mi kuŝiĝas sur la tapiŝon kaj kuŝas por iom da tempo. Nenial.]]
		end;
	end;
};

room {
	pic = "gfx/blanka-cxambro.png";
	nam = "whiteroom";
	disp = "Blanka ĉambro";
	dsc = [[Mi troviĝas en blanka ĉambro sen fenestroj. Ĝi aspektas pura, sed ŝajnas, ke neniu vivas ĉi tie.]];
	obj = {"armchair", "table_whiteroom", "carpet"};
	decor = [[Mi vidas kelkajn pordojn, per kiuj mi eble povas iri ien ajn, se mi volus.]];
	way = {path {"Loko de vekiĝo", "main"}, "hallway", "bathroom", "outside-whiteroom"};
};
