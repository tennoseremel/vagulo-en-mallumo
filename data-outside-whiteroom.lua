obj {
	nam = "person";
	dsc = [[En unu el la seĝoj sidas {person|svelta blondulo}.
			Ĝi estas vestita per hela ĝinzo kaj blanka varma bluzo, kaj aspektas ĉirkaŭ dudek jarojn aĝa.]];
	act = function(s)
		walkin "person_dialog";
	end;
};

obj {
	nam = "table_outside_whiteroom";
	disp = "tableto";
	dsc = [[En la dekstra parto de ĝardeno staras du molaj seĝoj kaj {table_outside_whiteroom|tableto} inter ili.]];
	act = [[La tableto estas iomete varma. Neatendite.]];
};

room {
	pic = "gfx/gxardeno.png";
	nam = "outside-whiteroom";
	disp = "Pordo eksteren";
	title = "Ĝardeno de deziroj";
	dsc = [[Trans la pordo estas bela ĝardeno.]];
	obj = {"table_outside_whiteroom", "person"};
	way = {"whiteroom"};
	words_shown = false;
	onenter = function(s)
		-- TODO: check condition
		p "La pordo estas fermita. Mi ne scias, kiel malfermi ĝin, ĉar ĝi eĉ ne havas serurtruon.";
		return false;
	end;
	enter = function(s)
		if not s.words_shown then
			if INFO_clothes_on then
				p [[«Tamen vi venis», mi aŭdas.]];
			else
				p [[«Tamen ci venis», mi aŭdas.]];
			end;
			s.words_shown = true;
		end;
	end;
};

dlg {
	nam = "person_dialog";
	name_known = false;
	observed_by_player = false;
	title = function(s)
		if s.name_known then
			return "Interparolo kun Naren'";
		else
			return "Interparolo kun nekonato";
		end;
	end;
	noinv = true;
	enter = [[— Bonvenon. Mi esperas, ke vi ŝatis mian domon ĉi-foje.]];
	phr = {
		{
			cond = function() return not _("person_dialog").name_known; end,
			"Saluton? Kiu vi estas?",
			function()
				_("person_dialog").name_known = true;
				if INFO_clothes_on then
					p [[— Oni nomas min Naren'. Bonvolu sidiĝi, vi verŝajne estas laca.]];
				else
					p [[— Oni nomas min Naren'. Sidiĝu, la seĝoj ĉi tie ĉiam estas varmaj pro amuza sorĉo.]];
				end;
				 p [[^^Mi sidiĝas sur neokupitan seĝon.]];
			end
		},
		{
			cond = function() return _("person_dialog").name_known; end,
			"Pririgardi Narenon.",
			function()
				_("person_dialog").observed_by_player = true;
				p [[Mi atente pririgardas Narenon, dume ĝi rigardas rekte al mi.
					Ĝia bela vizaĝo ŝajnas juna, sed ĉu tio signifas ion ajn en ĉi tiu stranga loko?
					Ĝi ankaŭ aspektas iomete pli virina, ol vira, sed mi ankaŭ ne certas pri tio.]];
				if INFO_clothes_on then
					p [[^^— Vi ĉiufoje estas bela, — subite diras Naren'.]];
				else
					p [[^^— Ci ĉiufoje estas bela, — subite diras Naren'.]];
				end;
			end
		},
		{
			cond = function() return _("person_dialog").observed_by_player; end,
			"Ĉu vi scias min?",
			function()
				if INFO_clothes_on then
					p [[— Eble.]];
				else
					p [[— Verŝajne.]];
				end;
			end
		},
		{
			cond = function() return _("person_dialog").name_known; end,
			"Kial mi estas ĉi tie?",
			function()
				p [[— Nu, kiam homoj mortas, iliaj animoj iufoje trapasas mian domon.
					Mi ne certas kial, ĉar kutime nur feoj vivas ĉi tie.
					Antaŭ tre longe feoj trovis la lokon, kaj decidis vivi en ĝi.]];
				if INFO_clothes_on then
					p [[Kaj vi estas ĉi tie, ĉar vi mortis, kompreneble.]];
				else
					p [[Kaj ci estas ĉi tie, ĉar ci mortis, kompreneble.]];
				end;
			end
		},
	};
};
