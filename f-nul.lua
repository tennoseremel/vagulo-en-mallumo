room {
	pic = "gfx/librejo.png";
	nam = "f_nul";
	disp = "Librejo";
	noinv = true;
	dsc = function(s)
		p [[Mi eniras en la librejon.
			^^Tik-tak, tik-tak, tik-tak… Kia freneza sono. De kie ĝi sonas?
			La tuta loko eĉ ne havas horloĝojn! Ĉu mi nur imagas tion?
			Aŭ ĉu tio estas la sono de mia foriranta tempo?
			^^Mi lace sidas en la brakseĝon kaj fermas la okulojn.
			^^Iom post iom mi sentas, ke la lumo malgrandiĝas.
			Eble tio ankaŭ estas nur mia imago, tamen mi vere ne havas forton por malfermi la okulojn.
			Mi volas dormi. Iomete. Mi devas dormi. Post tio mi esploros. Eble…
			^^Malrapide la tiktako malproksimiĝas. Bone. Mi ne bezonas ĝin. Mi dormos iomete pli…
			^^…^^…^^…^^]];
		p(fmt.c(fmt.b('FINO NUL:')));
		p(' Kutima ciklo.');
	end;
};
