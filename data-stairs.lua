obj {
	nam = "cat1";
	disp = "nigra kato";
	dsc = [[{cat1|Nigra kato} dormas sur {stairs_window|fenestro}breto.]];
	act = [[Nigra kato mallaŭte miaŭas.]];
};

obj {
	nam = "cat2";
	disp = "oranĝa kato";
	dsc = [[{cat2|Oranĝa kato} sidas sur la {washing_machine|lavmaŝino}, rigardante min.]];
	act = [[Oranĝa kato ronronas.]];
};

obj {
	nam = "cat3";
	disp = "stria kato";
	dsc = [[{cat3|Stria kato} promenas sur la plafono.]];
	act = [[Stria kato troviĝas tro alte.]];
};

obj {
	nam = "stairs_window";
	disp = "fenestro";
	act = [[Tra la fenestro mi vidas nur senvivan teron sub oranĝkolora ĉielo.
			La tero havas oranĝan aŭ eble brunan koloron. En iuj lokoj ĝi estas kovrita per polvo.
			Nur ŝtonoj kaj polvo, nenio alia ekzistas tie.
			^^La fenestro mem ne havas ion ajn, per kiu mi povus malfermi ĝin.
			Eble mi povus disbati ĝin, sed tio sonas kiel tre malbona ideo.]];
};

obj {
	nam = "bracelet";
	disp = "ĉirkaŭmano";
	inv = [[«Verŝajneco» estas skribita sur la ĉirkaŭmano.]];
	use = function(s, other)
		if other.nam == "cat1" then
			p [[La kato daŭrigas sian dormon.]];
		elseif other.nam == "cat2" then
			p [[La kato nekomprenante rigardas min.]];
		elseif other.nam == "cat3" then
			p [[La kato troviĝas tro alte.
				^^— Saluton, kato! Lo!
				^^La kato ne respondas. Katoj…]];
		elseif other.nam == "glowing_dots" then
			p [[Mi aŭdas du voĉojn, sed mi ne komprenas la lingvon.]];
		else
			p [[Kial?]];
		end;
	end;
};

obj {
	nam = "washing_machine";
	disp = "lavmaŝino";
	took_item = false;
	act = function(s)
		if s.took_item then
			p [[Lavmaŝino ne funkcias.]];
		else
			p [[Lavmaŝino ne funkcias. Ene mi trovas ĉirkaŭmanon.]];
			take "bracelet";
			s.took_item = true;
		end;
	end;
};

room {
	pic = "gfx/sxtuparo.png";
	nam = "stairs";
	disp = "Ŝtuparo supren";
	title = "La dua etaĝo";
	dsc = [[Mi nun troviĝas en la koridoro de la dua etaĝo.
			Al mi ĝi ŝajnas iomete konata, sed samtempe malĝusta.]];
	decor = [[{tiktako|Tik-tak}…
			^^Mi vidas tri katojn ĉi tie. Kiu manĝigas ilin?]];
	obj = {"tiktako", "cat1", "cat2", "washing_machine", "cat3", "stairs_window"};
	tiktako_increase = true;
	way = {"library", "room_of_fear"};
};
