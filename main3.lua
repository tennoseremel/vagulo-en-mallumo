-- $Name: Vagulo en mallumo$
-- $Version: 0.10$
-- $Author: Tenno Seremel$
-- $Info: La ludo uzas muzikon de [Jason Shaw] el audionautix.com (CC BY 4.0)\nVi povas uzi la kodon kaj la historion de la ludo sub licenco CC0 1.0 Universala.$
require "snd";
require "noinv";
require "fmt";
global {
	INFO_central_room = "search";
	INFO_clothes_on = true;
}

game.act = "Interese.";
game.use = "Tio ne funkcias tiumaniere.";
game.inv = "Tre interese.";
std.phrase_prefix = "— ";

function get_central_disp()
	return _(INFO_central_room).disp;
end

function get_central_nam()
	return INFO_central_room;
end

include "data-main";
include "data-search";
include "data-library";
include "data-whiteroom";
include "data-bathroom";
include "data-outside-whiteroom";
include "data-outside-library";
include "data-hallway";
include "data-stairs";
include "data-roomoffears";
include "f-nul.lua";

snd.music("mus/atlantis.mp3");
