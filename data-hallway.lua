obj {
	nam = "mirror";
	disp = "spegulo";
	notebook_activated = false;
	act = [[Mi garde tuŝas spegulon. La spegulaĵo ankaŭ moviĝas kaj «tuŝas» min de alia flanko de la spegulo.]];
};

room {
	pic = "gfx/koridoro.png";
	nam = "hallway";
	disp = "Koridoro";
	title = "Spegula koridoro";
	dsc = [[Mi paŝas tra stranga loko.]];
	decor = function(s)
		p [[La muroj, la planko kaj la plafono estas {mirror|speguloj}. Multe da]];
		if not INFO_clothes_on then
			p [[senvestitaj]];
		end;
		p [[virinoj de diversaj aĝoj speguliĝas en ĝi. Ili ĉiuj estas mi. Verŝajne.
			^^{tiktako|Tik-tak}…]];
	end;
	obj = {"tiktako", "mirror"};
	way = {"whiteroom"};
};
