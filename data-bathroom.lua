obj {
	nam = "water";
	disp = "akvo";
	hot = false;
	dsc = function(s)
		if s.hot then
			p [[Ĝi estas plena de {water|varma akvo}.]];
		else
			p [[Ĝi estas plena de {water|akvo}.]];
		end;
	end;
	act = function(s)
		if s.hot then
			p [[Ĝi estas sufiĉe varma. Mi certe povas senti tion!]];
		else
			p [[Ŝajnas, ke ĝi estas malvarma. Ĉu mi nur imagas tion?]];
		end;
	end;
};

obj {
	nam = "chair";
	disp = "pufseĝo";
	dsc = [[Malgranda {chair|pufseĝo} staras dekstre.]];
	obj = {"clothes"};
};

obj {
	nam = "clothes";
	disp = "vesto";
	dsc = function(s)
		local messages = {
			default_message = "La {clothes|vesto} kuŝas sur ĝi.",
			bath = "La malseka vesto naĝas en la akvo.",
			table_outside_whiteroom = "Sur la tableto kuŝas mia vesto.",
			washing_machine = "En la lavmaŝino kuŝas mia {clothes|vesto}."
		};
		local where_is = where(s);
		local message;
		if where_is == nil then
			message = messages["default_message"];
		else
			message = messages[where_is.nam];
			if message == nil then
				message = messages["default_message"];
			end;
		end;
		p(message);
	end;
	tak = [[Mi prenas la veston.]];
	inv = function(s)
		if _("towel").towel_used then
			p [[Mi vestas min.]];
		else
			p [[Mi surmetis veston ĝuste sur mia malseka korpo.]];
		end;
		INFO_clothes_on = true;
		remove(s);
	end;
	use = function(s, other)
		if other.nam == "chair" then
			p [[Mi metas ĝin sur la pufseĝon.]];
			place(s, "chair");
		elseif other.nam == "bath" or other.nam == "water" then
			p [[Mi forĵetas la veston en la akvon.]];
			place(s, "bath");
		elseif other.nam == "table_outside_whiteroom" then
			p [[Mi metas la veston sur la tableton, tamen la blondulo diras nenion.
				Nu, mi mem ne certas, kial mi faris tion.]];
			place(s, "table_outside_whiteroom");
		elseif other.nam == "washing_machine" then
			p [[Mi metas ĝin en la lavmaŝinon. Bone, sed la lavmaŝino ne funkcias.]];
			place(s, "washing_machine");
		else
			p [[Mi ne volas.]];
		end;
	end;
}:disable();

obj {
	nam = "towel";
	disp = "bantuko";
	dsc = [[Maldekstre pendas ruĝa {towel|bantuko}.]];
	towel_used = false;
	act = function(s)
		if _("bath").bath_used and not INFO_clothes_on and not s.towel_used then
			s.towel_used = true;
			p [[Mi viŝas min. Agrable.]];
		else
			p [[Eble la blua estus pli bela.]];
		end;
	end;
};

obj {
	nam = "bath";
	disp = "banujo";
	bath_used = false;
	dsc = "Blanka {bath|banujo} okupas duonon de la ĉambro.";
	act = function(s)
		if s.bath_used then
			p "Ne-e…";
		elseif _("water").hot then
			s.bath_used = true;
			INFO_clothes_on = false;
			_("clothes"):enable();
			p [[Mi banas min dum iom da tempo. Senti ion ajn estis tre agrable.]];
		else
			p [[Ĉu mi povas?..]];
		end;
	end;
	obj = {"water"};
};

room {
	pic = "gfx/banejo.png";
	nam = "bathroom";
	disp = "Banĉambro";
	dsc = function(s)
		if _("bath").bath_used then
			p [[Komforta banĉambro. Ĉu mi bezonas iri ien ajn?]];
		else
			p [[Mi sentas min laca…]];
		end;
	end;
	way = {path {nam = "#tocentral", get_central_disp, get_central_nam}};
	obj = {"bath", "towel", "chair"};
	exit = function(s)
		if not INFO_clothes_on then
			p "Mi sentas min vento.";
		end;
	end;
};
