obj {
	nam = "books";
	disp = "libroj";
	dsc = [[Estas multege da {books|libroj} ĉi tie, oni facile povas perdiĝi.]];
	act = [[Literoj en ĉi tiuj libroj ŝajnas nekonataj al mi. Ili ankaŭ havas bildojn de strangaj bestoj kaj figuroj.]];
};

obj {
	nam = "photo1";
	disp = "fotaĵo de maljuna viro";
	dsc = [[En la maldesktra flanko de la tablo kuŝas {photo1|fotaĵo de maljuna viro}.]];
	inv = [[Eble me konis lin.]];
	tak = [[Mi prenas la fotaĵon.]];
	use = function (s, other)
		if other.nam == "table_library" then
			p [[Mi metas ĝin sur la tablon.]];
			place(s, "table_library");
		elseif other.nam == "glowing_dots" then
			p [[Nenio okazis.]];
		else
			p [[Ŝajne, ne.]]
		end
	end;
};

obj {
	nam = "photo2";
	disp = "fotaĵo de juna virino";
	dsc = [[En la mezo de la tablo kuŝas {photo2|fotaĵo de juna virino}.]];
	inv = [[Mi ne certas, kiu ŝi estas.]];
	tak = [[Mi prenas la fotaĵon.]];
	use = function (s, other)
		if other.nam == "table_library" then
			p [[Mi metas ĝin sur la tablon.]];
			place(s, "table_library");
		elseif other.nam == "glowing_dots" then
			p [[«Evidente, tio funkcias nur kontraŭ ordinaraj homoj», mi aŭdas. Kio?]];
		else
			p [[Ŝajne, ne.]]
		end
	end;
};

obj {
	nam = "table_library";
	disp = "tablo";
	dsc = [[Ligna {table_library|tablo} kun komforta brakseĝo apude staras dekstre de mi.]];
	act = [[Ĝi aspektas tre peza. Mi ŝatas ĝin.]];
	obj = {"photo1", "photo2"};
};

obj {
	nam = "glowing_dots";
	disp = "brilantaj punktoj";
	dsc = [[Iom da malgrandaj {glowing_dots|brilantaj punktoj} naĝas en la aero de malproksima parto de librejo.]];
	act = [[Mi aliras al unu brilanta punkto kaj tuŝas ĝin. Nedistingebla flustrado eksonas el ĝi. Nenio alia okazas.]];
};

room {
	pic = "gfx/librejo.png";
	nam = "library";
	disp = "Librejo";
	dsc = [[Mi troviĝas en bela librejo sen fenestroj. Ĉu iu ajn estas ĉi tie?]];
	obj = {"books", "glowing_dots", "table_library"};
	decor = [[Mi vidas kelkajn pordojn, per kiuj mi eble povas iri ien ajn, se mi volus.]];
	way = {path {"Loko de vekiĝo", "main"}, "bathroom", "outside-library", "stairs"};
	onenter = function(s)
		if _('tiktako').tik_tak_count > 3 and not _("bath").bath_used then
			walk('f_nul', false, false);
			return false;
		end
	end;
};
