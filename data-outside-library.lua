room {
	pic = "gfx/mallumo.png";
	nam = "outside-library";
	disp = "Pordo eksteren";
	title = "Ekstero";
	dsc = [[Mi ne povas vidi ion ajn. Denove mallumo estas ĉie…]];
	way = {"library"};
	onenter = function(s)
		p "La pordo estas fermita. Mi ne scias, kiel malfermi ĝin, ĉar ĝi eĉ ne havas serurtruon.";
		return false;
	end;
};
